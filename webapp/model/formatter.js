sap.ui.define([], function () {
	"use strict";
	return {

		statusFormatter: function (status) {
			let value = "";
			switch (status) {
				case "PDO":
					value = "Изходяща";
					break;
				case "PDI":
					value = "Входяща";
					break;
				default:
					value = status;
			}
			return value;
		},

		isEnabledFormatter: function (status, val, lockInput) {
			if (status === "PDO" && val !== "0.000") {
				if (lockInput) {
					return false;
				}
				return true;
			} else if (status === "PDI") {
				return false;
			} else {
				return false;
			}
		},

		statusFormatterStats: function (status) {
			let value = "";
			switch (status) {
				case "04":
					value = "Активен";
					break;
				case "06":
					value = "Товарене";
					break;
				case "09":
					value = "Приключен";
					break;
				default:
					value = status;
			}
			return value;
		},

		statusInputFormatter(val) {
			return 5;
		}

	};
});
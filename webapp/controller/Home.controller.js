sap.ui.define([
	"./Base.controller",
	"../model/formatter",
	'sap/m/Dialog',
	"sap/ui/model/json/JSONModel",
	"sap/ui/core/Fragment",
], function (Base, formatter, Dialog, JSONModel, Fragment) {
	"use strict";

	return Base.extend("zfioriscaletwo.controller.Home", {

		formatter: formatter,
		tableRowsLengh: undefined,
		fullModelData: undefined,
		filteredModelData: undefined,
		currentChosenWarehouseNumber: undefined,

		onInit: function () {
			let currentRouter = sap.ui.core.UIComponent.getRouterFor(this);
			currentRouter.getRoute("home").attachPatternMatched(this.onHomeMatched, this);
			this.initSelectedWhNumber();
			this.getComputersInformationData()

		},

		getComputersInformationData: function () {
			sap.ui.core.BusyIndicator.show();
			const oModel = new sap.ui.model.odata.v2.ODataModel("/sap/opu/odata/sap/ZYL_YARD_UI_SRV");
			oModel.read("/DNSPCNameCollection", {
				success: (data) => {
					sap.ui.core.BusyIndicator.hide();
					const jsonModelPCItemsData = new JSONModel({ "items": data.results });
					setTimeout(() => (this.onOpenSetScaleDialog(jsonModelPCItemsData)), 0);
				},
				error: err => {
					sap.ui.core.BusyIndicator.show();
					this.handleMessageResponse(err);
				}
			});
		},

		onOpenSetScaleDialog: function (jsonModelPCItemsData) {
			const view = this.getView();
			const popupDialogZero = this.byId("popupDialogSetScale")
			if (!popupDialogZero) {
				Fragment.load({
					id: view.getId(),
					name: "zfioriscaletwo.view.dialogs.PopupDialogSetScale",
					controller: this
				}).then((popupDialogZero) => {
					view.addDependent(popupDialogZero);
					popupDialogZero.setModel(jsonModelPCItemsData, "pcItems");
					popupDialogZero.open();
				});
			} else {
				const dialog = this.byId("popupDialogSetScale");
				dialog.setModel(jsonModelPCItemsData, "pcItems");
				dialog.open();
			}
		},

		onCloseSetScale: function () {
			const popupDialogZero = this.byId("popupDialogSetScale")
			const comboBox = this.getView().byId("scaleComboBox");
			const selectedKey = comboBox.getSelectedItem().getKey()
			if (selectedKey) {
				this.saveDataToLocalModel("selectedScaleKeyDNS", selectedKey);
				popupDialogZero.close();
			} else {
				sap.m.MessageToast.show("Изберете кантар!");
			}

		},

		onHomeMatched: function () {
			const oModel = this.getDataFromLocalModel("lastModel");
			if (oModel) {
				const uiTable = this.getView().byId("uiTable");
				uiTable.setModel(oModel, "uiTableModel");
			}
			const datesArr = this.getDataFromLocalModel("dates");
			if (datesArr) {
				let beginDatePicker = this.getView().byId("beginDate");
				let endDatePicker = this.getView().byId("endDate");
				beginDatePicker.setDateValue(datesArr[0]);
				endDatePicker.setDateValue(datesArr[1]);
			}

			// reset block stock indicator to false
			this.saveDataToLocalModel("blockStock", false);

			this.setFormattedColumnsFilter();
			this.getStorageBinSearchHelp();
			this.onGetDataPressed();
		},

		setFormattedColumnsFilter: function () {
			const columns = {
				"statusColumn": this.getView().byId("statusColumn"),
				"deliveryColumn": this.getView().byId("deliveryColumn")
			};
			let statusObject = {
				"АКТИВЕН": "04",
				"ПРИЛКЛЮЧЕН": "09",
				"ИЗХОДЯЩА": "PDO",
				"ВХОДЯЩА": "PDI"
			};

			Object.values(columns).forEach(column => {
				column.setFilterType(function (value) {
					let valueToUpper = value.toUpperCase();
					for (let key in statusObject) {
						if (key.includes(valueToUpper)) {
							return statusObject[key]
						}
					}
				});
			});
		},

		saveDataToLocalModel: function (key, value) {
			const localModel = this.getView().getModel("localModel");
			let currentObject = localModel.getData();
			currentObject[key] = value;
			localModel.setData(currentObject);
		},

		getDataFromLocalModel: function (key) {
			const localModel = this.getView().getModel("localModel");
			let currentObject = localModel.getData();
			return currentObject[key];
		},

		onGetDataPressed: function () {

			const oModel = new sap.ui.model.odata.v2.ODataModel("/sap/opu/odata/sap/ZYL_YARD_UI_SRV", {
				useBatch: false
			});
			const table = this.getView().byId("uiTable");
			let startDate = this.getView().byId("beginDate").getDateValue();
			let endDate = this.getView().byId("endDate").getDateValue();

			if (!startDate || !endDate) {
				sap.m.MessageToast.show("Изберете дати!");
			} else {
				table.setBusy(true);
				let lgNum = new sap.ui.model.Filter("Lgnum", "EQ", this.currentChosenWarehouseNumber);
				let dateFrom = new sap.ui.model.Filter("SelectDateFrom", "EQ", startDate);
				let dateTo = new sap.ui.model.Filter("SelectDateTo", "EQ", endDate);

				this.saveDataToLocalModel("dates", [startDate, endDate]);
				const arr = [lgNum, dateFrom, dateTo];
				oModel.read("/ShipDeliveryGenSet", {
					filters: arr,
					success: (data) => {
						let resultData = data.results;
						this.fullModelData = new JSONModel({ "items": resultData });
						this.tableRowsLengh = resultData.length;
						let filteredArray = resultData.filter(item => item.Status !== "09");
						this.filteredModelData = new JSONModel({ "items": filteredArray });
						table.setModel(this.filteredModelData, "uiTableModel");
						if (this.getView().byId("hideDuplicatesCheckBox").getSelected()) {
							this.hideDuplicates();
						} else {
							table.setVisibleRowCount(filteredArray.length);
						}
						this.saveDataToLocalModel("lastModel", this.filteredModelData);
						table.setBusy(false);
						this.quickFilterOnTable("nonConfirmed");
					},
					error: err => table.setBusy(false)
				});
			}
		},

		onRowNavigationPressed: function (oEvent, cellEvent) {
			let dataEvent = {};
			if (oEvent) {
				dataEvent = oEvent.getSource().getBindingContext("uiTableModel");
			} else {
				dataEvent = cellEvent.getParameter("rowBindingContext");
			}
			let lgNum = dataEvent.getProperty("Lgnum");
			let torKey = dataEvent.getProperty("TorKey");
			let status = dataEvent.getProperty("Status");

			const oRouter = sap.ui.core.UIComponent.getRouterFor(this);
			oRouter.navTo("scale", {
				rowData: JSON.stringify([lgNum, status])
			});
			this.saveDataToLocalModel("torKey", torKey);
			this.saveDataToLocalModel("destinationBin", "");
		},

		onCellClick: function (cellEvent) {
			this.onRowNavigationPressed(undefined, cellEvent)
		},

		onAddCardPressed: function () {
			const table = this.getView().byId("uiTable");
			const selectedIndices = table.getSelectedIndices();
			if (selectedIndices.length >= 1) {
				const dialog = new Dialog({
					title: 'Добави карта',
					type: 'Message',
					content: [
						new sap.m.Label({ text: 'Моля въведете или сканирайте карта!', labelFor: 'submitDialogTextarea' }),
						new sap.m.TextArea('submitDialogTextarea', {
							liveChange: (oEvent) => {
								let sText = oEvent.getParameter('value');
								let parent = oEvent.getSource().getParent();
								parent.getBeginButton().setEnabled(sText.length > 0 && sText.length <= 15);

							},
							width: '100%',
						})
					],
					beginButton: new sap.m.Button({
						type: "Emphasized",
						text: 'Потвърди',
						enabled: false,
						press: () => {
							const tableModel = table.getModel("uiTableModel");
							let arr = tableModel.getData().items;
							let firstSelectedVisibleRowIndex = table.getContextByIndex(selectedIndices[0]).sPath.split("/")[2];

							let oldWord = arr[firstSelectedVisibleRowIndex].DriverName;
							for (let selectedIndex of selectedIndices) {
								let visibleSelectedIndex = table.getContextByIndex(selectedIndex).sPath.split("/")[2];

								let driver = arr[visibleSelectedIndex].DriverName;
								if (oldWord !== driver) {
									sap.m.MessageToast.show("Избрани са различни шофьори!");
									return;
								}
							}
							let number = sap.ui.getCore().byId('submitDialogTextarea').getValue();
							selectedIndices.forEach(index => {
								let visibleSelectedIndex = table.getContextByIndex(index).sPath.split("/")[2];
								tableModel.getData().items[visibleSelectedIndex].CardNo = number;
								tableModel.refresh();
							});
							dialog.close();
							this.onAddCardNumberPressed();
						}
					}),
					endButton: new sap.m.Button({
						text: 'Върни се',
						press: () => dialog.close()
					}),
					afterClose: () => dialog.destroy()

				});
				dialog.addStyleClass("sapUiSizeCompact");
				dialog.open();
			} else {
				sap.m.MessageToast.show("Моля изберете поне 1 ред!")
			}
		},

		onAddCardNumberPressed: function () {
			const oModel = new sap.ui.model.odata.v2.ODataModel("/sap/opu/odata/sap/ZYL_YARD_UI_SRV", {
				useBatch: false
			});
			const table = this.getView().byId("uiTable");
			let selectedIndices = table.getSelectedIndices();
			let selectedItems = [];
			selectedIndices.forEach(index => {
				let visibleSelectedIndex = table.getContextByIndex(index).sPath.split("/")[2];
				selectedItems.push(table.getModel("uiTableModel").getData().items[visibleSelectedIndex]);
			});
			let requestBody = {};
			requestBody.ShipDeliveryGenSetNav = selectedItems;

			oModel.create("/ShipDeliveryGenSet", requestBody, {
				success: (successData, response) => {
					sap.m.MessageToast.show("Данните са обновени!");
				},
				error: err => sap.m.MessageToast.show(JSON.parse(err.responseText).error.message.value)
			});
		},

		onCheckBoxSelected: function (evt) {
			if (evt.getSource().getSelected()) {
				this.hideDuplicates();
			} else {
				this.onGetDataPressed();
			}
		},

		onShowAllPressed: function () {
			const table = this.getView().byId("uiTable");
			table.setModel(this.fullModelData, "uiTableModel")
			this.quickFilterOnTable("all");
			if (this.getView().byId("hideDuplicatesCheckBox").getSelected()) {
				this.hideDuplicates();
			}
			table.setVisibleRowCount(this.tableRowsLengh);
		},

		onQualityInspPressed: function () {
			const table = this.getView().byId("uiTable");
			const selectedItemsIndices = table.getSelectedIndices();

			if (selectedItemsIndices.length == 1) {
				// 1 row was selected
				const tableData = table.getModel("uiTableModel").getData();
				let firstSelectedVisibleRowIndex = table.getContextByIndex(selectedItemsIndices[0]).sPath.split("/")[2];

				const rowData = tableData.items[firstSelectedVisibleRowIndex];
				const qInsp = (rowData.Doccat === 'PDO') ? true : false;

				const context = this;
				let dialogContent = [];

				// build dialog content
				const binLabel = new sap.m.Label("binLabel", {
					text: "Дестинация: "
				});
				const inputBin = new sap.m.Input("binInput", {
					showSuggestion: true,
					enabled: true,
					width: "100%"
				});
				const blockCheckBox = new sap.m.CheckBox("blockCheckBox", {
					text: "Блокирай стоката",
					selected: !qInsp,
					editable: qInsp
				});

				const binHelpArr = this.getView().getModel("storageBinSeachHelp").getData();
				for (let i = 0; i < binHelpArr.length; i++) {
					inputBin.addSuggestionItem(new sap.ui.core.Item(
						{ text: binHelpArr[i].StorageBin }
					));
				};

				dialogContent.push(binLabel);
				dialogContent.push(inputBin);
				dialogContent.push(blockCheckBox);

				const oDialog = new sap.m.Dialog({
					title: "Прoверка на качеството",
					type: "Message",
					content: dialogContent,
					beginButton: new sap.m.Button({
						type: sap.m.ButtonType.Emphasized,
						text: "Изпрати",
						enabled: true,
						press: function () {
							const inputValue = inputBin.getValue();
							const blockStock = blockCheckBox.getSelected();
							const validBinFlag = binHelpArr.find(data => data.StorageBin === inputValue);

							if (inputValue && validBinFlag) {
								const oRouter = sap.ui.core.UIComponent.getRouterFor(context);
								if (qInsp) {
									// outbound dlv => navigate to quality insp. screen
									oRouter.navTo("qinsp", {
										rowData: JSON.stringify([rowData.Lgnum, rowData.Status])
									});
								} else {
									// inbound dlv => navigate to standard scale screen
									oRouter.navTo("scale", {
										rowData: JSON.stringify([rowData.Lgnum, rowData.Status])
									});
								}

								// save the entered bin and block indicator to local model
								context.saveDataToLocalModel("destinationBin", inputValue);
								context.saveDataToLocalModel("blockStock", blockStock);
								context.saveDataToLocalModel("torKey", rowData.TorKey);
								oDialog.close();
							} else {
								sap.m.MessageBox.alert("Въведете валидна дестинация!");
							}
						}
					}),
					endButton: new sap.m.Button({
						text: "Затвори",
						press: function () {
							oDialog.close();
						}
					}),
					afterClose: function () {
						oDialog.destroy();
					}
				});

				oDialog.open();

			} else {
				sap.m.MessageToast.show("Изберете един ред от таблицата!");
			}

		},

		quickFilterOnTable: function (key) {
			let table = this.getView().byId("uiTable");
			const filters = {
				"active": [new sap.ui.model.Filter("Status", "EQ", "04")],
				"confirmed": [new sap.ui.model.Filter("Status", "EQ", "09")],
				"nonConfirmed": [new sap.ui.model.Filter("Status", "NE", "09")],
				"all": []
			};
			let oBinding = table.getBinding();
			let filterKey = key ? key : "all"
			oBinding.filter(filters[filterKey]);
		},


		hideDuplicates: function () {
			const table = this.getView().byId("uiTable");
			const arrItems = table.getModel("uiTableModel").getData().items
			let firstItem = arrItems[0];
			let uniqArr = [firstItem];
			arrItems.forEach(item => {
				let key = createCustomKey(item);
				if (key !== createCustomKey(firstItem)) {
					uniqArr.push(item);
					firstItem = item;
				}
			});
			table.getModel("uiTableModel").getData().items = uniqArr;
			table.getModel("uiTableModel").refresh();
			table.setVisibleRowCount(uniqArr.length);

			function createCustomKey(item) {
				return item.Doccat + item.DriverName + item.TruckNo + item.CardNo + item.ShipNo;
			}
		},

		getStorageBinSearchHelp: function () {
			const lgNum = new sap.ui.model.Filter("Lgnum", "EQ", this.currentChosenWarehouseNumber);
			const arr = [lgNum];
			const oModel = new sap.ui.model.odata.v2.ODataModel("/sap/opu/odata/sap/ZYL_YARD_UI_SRV", {
				useBatch: false
			});
			const context = this;

			oModel.read("/StorageBinSearchHelpSet", {
				filters: arr,
				success: (data) => {
					const resultData = data.results;
					const oModelHelpData = new JSONModel(resultData);
					context.getView().setModel(oModelHelpData, "storageBinSeachHelp");
				}
			});
		}

	});
});
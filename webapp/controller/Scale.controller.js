sap.ui.define([
    "./Base.controller",
    "../model/formatter",
    "sap/m/MessageToast",
    "sap/m/MessageBox",
    "sap/ui/model/odata/v2/ODataModel",
    "sap/ui/model/json/JSONModel",
    "sap/ui/model/Filter",
    "sap/ui/core/Fragment",
], function (Base, formatter, MessageToast, MessageBox, oDataModel, JSONModel, Filter, Fragment) {

    "use strict";

    return Base.extend("zfioriscalewo.controller.Scale", {

        formatter: formatter,
        isFinishButtonProcessActivated: undefined,
        lastPressedButtonArr: [],

        onInit: function () {
            let currentRouter = sap.ui.core.UIComponent.getRouterFor(this);
            currentRouter.getRoute("scale").attachPatternMatched(this.onRouteMatched, this);
        },

        recalculateInputData: function (event, elementId) {
            if (event) {
                elementId = event.getSource().sId;
            }
            sap.ui.core.BusyIndicator.show();
            const oModel = new oDataModel("/sap/opu/odata/sap/ZYL_YARD_UI_SRV", true);
            const tableModel = this.getView().getModel("table");
            const table = this.getView().byId("uiTableScale");
            let requestBody = {};
            let itemData = tableModel.getData().results[0];
            if (elementId.includes("quantityBag")) {

                this.getView().byId("quantityBagInputTable").getValue().toString();
                itemData.QuanDoc.Quan = "0";
            } else {

                itemData.QuanBag.Quan = "0";
            }
            requestBody = itemData

            oModel.create("/ShipDeliveryItemSet", requestBody, {
                success: (s, res) => {
                    sap.ui.core.BusyIndicator.hide();
                    table.getModel("table").getData().results[0] = s;
                    table.getModel("table").refresh();
                    this.handleMessageResponse(res);
                },
                error: err => {
                    sap.ui.core.BusyIndicator.hide();
                    this.handleMessageResponse(err);
                }
            });

        },

        onRouteMatched: function (oEvent) {
            sap.ui.core.BusyIndicator.show(0);
            const rowData = JSON.parse(oEvent.getParameter("arguments").rowData);
            const torKey = this.getDataFromLocalModel("torKey");
            this.checkStatus(rowData[1]);
            this.removeCssFromTable();
            if (torKey) {
                this.checkStatus(rowData[1]);
                this.getInputAutoCompleteData();
                this.getDataFromServer(rowData[0], torKey).then(serverData => {
                    const destBin = this.getDataFromLocalModel("destinationBin");
                    if (destBin) {
                        serverData.ShipDeliveryItemNav.results[0].Bin = destBin;
                    }
                    const jsonModelHeader = new JSONModel(serverData.ShipDeliveryHeadNav);
                    const jsonModelTable = new JSONModel(serverData.ShipDeliveryItemNav);

                    jsonModelTable.setProperty("/tableInputState", true)

                    this.getView().setModel(jsonModelHeader, "header");
                    this.getView().setModel(jsonModelTable, "table");

                    const areWeightInputsOpen = jsonModelHeader.getData().ManualWeight
                    this.isManualInputEnabled = areWeightInputsOpen
                    this.initSelectedWhNumber()
                    sap.ui.core.BusyIndicator.hide();
                    if (window.location.host.includes("app")) {
                        this.onTareSwitchChange(null, true)
                    }
                }).catch(err => {
                    this.onNavBack();
                    sap.ui.core.BusyIndicator.hide();
                });
                this.checkTableForConfirmedTask();
            } else {
                this.onNavBack();
                sap.ui.core.BusyIndicator.hide();
            }
        },

        checkTableForConfirmedTask: function () {
            const uiTable = this.getView().byId("uiTableScale");
            if (!uiTable.mEventRegistry.rowSelectionChange) {
                uiTable.attachRowSelectionChange((event) => {
                    let table = event.getSource();
                    table.getSelectedIndices().forEach(index => {
                        let isItemBlocked = table.getRows()[index].getBindingContext("table").getObject("BlockItem");
                        if (isItemBlocked) {
                            table.setSelectedIndex(-1)
                            table.getRows()[index]._setSelected(false);
                            MessageToast.show("За продукта има подтвърдена задача!");
                        }
                    });
                });
            }
        },

        onNavBack: function () {
            const oRouter = sap.ui.core.UIComponent.getRouterFor(this);
            oRouter.navTo("home");
        },

        getDataFromLocalModel: function (key) {
            const localModel = this.getView().getModel("localModel");
            let currentObject = localModel.getData();
            return currentObject[key];
        },

        getDataFromServer: function (lgNum, torKey) {
            return new Promise((resolve, reject) => {
                //---------------------
                const oModel = new oDataModel("/sap/opu/odata/sap/ZYL_YARD_UI_SRV", true);
                let setInfo = "(Lgnum='" + lgNum + "',TorKey=guid'" + torKey + "')"
                oModel.read("/ShipDeliveryGenSet" + setInfo, {
                    urlParameters: {
                        "$expand": "ShipDeliveryHeadNav,ShipDeliveryItemNav"
                    },
                    success: (data, res) => {
                        this.handleMessageResponse(res);
                        sap.ui.core.BusyIndicator.hide();
                        resolve(data);
                    },
                    error: err => {
                        sap.ui.core.BusyIndicator.hide();
                        this.handleMessageResponse(err);
                        reject(err);

                    }
                });
                //---------------------
            });
        },

        onWriteButtonPress: function () {
            this.sendHeaderDataToServerWithPostRequest();
        },

        sendHeaderDataToServerWithPostRequest: function () {
            const oModel = new oDataModel("/sap/opu/odata/sap/ZYL_YARD_UI_SRV", true);
            const headerModel = this.getView().getModel("header");
            let setInfo = "";
            let requestBody = headerModel.getData();
            sap.ui.core.BusyIndicator.show();
            const measuredButton = this.byId("topMeasureButton");
            measuredButton.setEnabled(false);
            oModel.create("/ShipDeliveryHeadSet" + setInfo, requestBody, {
                success: (s, res) => {
                    measuredButton.setEnabled(true);
                    sap.ui.core.BusyIndicator.hide();
                    this.getView().setModel(new JSONModel(s), "header");
                    this.handleMessageResponse(res);
                },
                error: err => {
                    measuredButton.setEnabled(true);
                    sap.ui.core.BusyIndicator.hide();
                    this.handleMessageResponse(err);
                }
            });
        },

        onTarePress: function () {
            sap.ui.core.BusyIndicator.show(0);
            const switchState = this.getView().byId("tareSwitch").getState()
            if (switchState) {
                this.fetchTareDataJSON().then(data => {
                    // When fetch succeeds
                    //send data to backend
                    const weight = data && data.currentWeight.match(/(\d+)\n?(\+?\-?)/)[0]
                    if (weight == "999") {
                        sap.ui.core.BusyIndicator.hide(0);
                        sap.m.MessageToast.show("Неконсистентни данни от кантара! Опитай отново!");
                    } else {
                        this.sendHeaderSetRequest(weight);
                    }

                }).catch(error => {
                    sap.ui.core.BusyIndicator.hide();
                    sap.m.MessageToast.show("Има проблем с данните от кантара!");
                });
            } else {
                const tareInput = this.getView().byId("tareInput");
                this.sendHeaderSetRequest(tareInput.getValue());

            }
        },
        sendHeaderSetRequest: function (scaleWeight) {
            const headerModel = this.getView().getModel("header");
            let torKey = headerModel.getData().TorKey;
            let lgNum = this.currentChosenWarehouseNumber;
            const weight = scaleWeight ? scaleWeight : "0"
            // new req ScaleQty
            const oModel = new oDataModel("/sap/opu/odata/sap/ZYL_YARD_UI_SRV", true);
            let setInfo = "(Lgnum='" + lgNum + "',TorKey=guid'" + torKey + "',ScaleQty='" + weight + "')";

            const tareButton = this.byId("topTareButton");
            tareButton.setEnabled(false);
            oModel.read("/ShipDeliveryHeadSet" + setInfo, {
                success: (s, res) => {
                    tareButton.setEnabled(true);
                    sap.ui.core.BusyIndicator.hide();
                    this.handleMessageResponse(res);
                    this.setTareValues(s);

                },
                error: err => {
                    tareButton.setEnabled(true);
                    sap.ui.core.BusyIndicator.hide(0);
                    this.handleMessageResponse(err);
                }
            });
        },


        setTareValues: function (values) {

            sap.ui.core.BusyIndicator.hide();
            const tareInput = this.getView().byId("tareInput");
            const grossInput = this.getView().byId("grossInput");
            const netInput = this.getView().byId("netInput");

            tareInput.setValue(values.TareWei.Quan);
            //tareInput.setDescription(data.TareWei.Unit);
            grossInput.setValue(values.GrossWei.Quan);
            //grossInput.setDescription(data.GrossWei.Unit);
            netInput.setValue(values.NetWei.Quan);
            //netInput.setDescription(data.NetWei.Unit);
        },



        onMeasuredPress: function () {
            this.sendHeaderDataToServerWithPostRequest();
        },

        // onWeightPressed: function () {
        //     const headerModel = this.getView().getModel("header");
        //     const lgNum = "DEV1";
        //     const torKey = headerModel.getData().TorKey;
        //     const table = this.getView().byId("uiTableScale");
        //     const dataArr = table.getModel("table").getData().results;
        //     const selectedIndices = table.getSelectedIndices();

        //     if (selectedIndices.length !== 1) {
        //         MessageToast.show("Избери 1 ред!")
        //     } else {
        //         const itemKey = dataArr[selectedIndices].ItmKey;
        //         let url = "/sap/opu/odata/sap/ZYL_YARD_UI_SRV/ScaleTaskItemSet(Lgnum='" + lgNum + "',TorKey=guid'" + torKey + "',ItmKey=guid'" + itemKey + "')?sap-client=300";
        //         const jsonModel = new JSONModel();
        //         jsonModel.loadData(url)
        //             .then(() => {
        //                 MessageToast.show("Успешно Теглене");
        //                 let quantityFull = jsonModel.getData().d.QuanWei;
        //                 let unit = quantityFull.Unit;
        //                 let quantity = quantityFull.Quan;
        //                 let cellCount = 3;
        //                 table.getRows()[selectedIndices].getCells()[cellCount].setText(quantity + " " + unit)
        //             })
        //             .catch(() => MessageToast.show("Неуспешно Теглене"))
        //     }
        // },

        onWeightPressed: function () {
            const cssClasses = {
                "03": "red-row",
                "02": "yellow-row"
            }
            const backEndEvent = "06";

            const table = this.getView().byId("uiTableScale");
            const selectedIndices = table.getSelectedIndices();

            const headerDataObj = this.getView().getModel("header").getData();
            if (selectedIndices.length !== 1) {
                MessageToast.show("Избери 1 ред!")
            } else {
                const oModel = new oDataModel("/sap/opu/odata/sap/ZYL_YARD_UI_SRV", true);
                const switchState = this.getView().byId("tareSwitch").getState()
                const dataArr = table.getModel("table").getData().results;
                if (switchState) {
                    this.fetchTareDataJSON().then(data => {
                        // When fetch succeeds
                        let weight = data && data.currentWeight.match(/(\d+)\n?(\+?\-?)/)[0]
                        if (weight == "999") {
                            sap.ui.core.BusyIndicator.hide();
                            sap.m.MessageToast.show("Неконсистентни данни от кантара! Опитай отново!");
                            return;
                        }
                        dataArr[selectedIndices].QuanWei.Quan = weight
                        table.getModel("table").refresh();
                        let requestBody = {}

                        requestBody.ShipDeliveryHeadNav = headerDataObj;
                        let selectedItem = dataArr[selectedIndices];
                        selectedItem.Task = backEndEvent;
                        requestBody.ShipDeliveryItemNav = [selectedItem];
                        this.createTaskGenRequest(oModel, requestBody, table, selectedIndices, cssClasses);
                    }).catch(error => {
                        // When fetch ends with a bad http status
                        console.log("problem with fetching data " + error)
                        sap.ui.core.BusyIndicator.hide();
                        sap.m.MessageToast.show("Има проблем с данните от кантара!");
                    });
                } else {
                    let requestBody = {}
                    requestBody.ShipDeliveryHeadNav = headerDataObj;
                    let selectedItem = dataArr[selectedIndices];
                    selectedItem.Task = backEndEvent;
                    requestBody.ShipDeliveryItemNav = [selectedItem];
                    this.createTaskGenRequest(oModel, requestBody, table, selectedIndices, cssClasses);
                }
            }
        },

        createTaskGenRequest: function (oModel, requestBody, table, selectedIndices, cssClasses) {
            const weightButton = this.byId("weightButton");
            weightButton.setEnabled(false);
            oModel.create("/ShipDeliveryGenSet", requestBody, {
                success: (successData, response) => {
                    weightButton.setEnabled(true);
                    const itemData = successData.ShipDeliveryItemNav.results[0];
                    const css = itemData.TolerStat;
                    this.removeCssFromTable();
                    table.getRows()[selectedIndices].addStyleClass(cssClasses[css]);

                    this.getView().setModel(new JSONModel(successData.ShipDeliveryHeadNav), "header");
                    this.getView().getModel("header").refresh(true);
                    table.getModel("table").getData().results[selectedIndices] = itemData;
                    table.getModel("table").refresh();
                    this.handleMessageResponse(response)

                },
                error: err => {
                    weightButton.setEnabled(true)
                    this.handleMessageResponse(err)
                }
            });
        },

        removeCssFromTable: function () {
            const cssClasses = { "03": "red-row", "02": "yellow-row" }
            const table = this.getView().byId("uiTableScale");
            table.getRows().forEach(row => {
                row.removeStyleClass(cssClasses["02"]);
                row.removeStyleClass(cssClasses["03"]);
            });
        },

        onSetToZeroPressed: function (event) {
            const button = event.getSource();
            const context = this;
            sap.ui.define(["sap/m/MessageBox"], function (MessageBox) {
                MessageBox.show(
                    "Сигурни ли сте, че искате да нулирате задачата?", {
                    icon: MessageBox.Icon.INFORMATION,
                    title: "Потвърждение",
                    actions: ["Да", "Не"],
                    emphasizedAction: MessageBox.Action.YES,
                    onClose: function (oAction) {
                        if (oAction === "Да") {
                            context.onEventButtonPressed("13", false, null, null, button);
                        }
                    }
                }
                );
            });
        },

        onTaskPressed: function (event) {
            const button = event.getSource();
            this.onEventButtonPressed("01", false, null, null, button);
        },
        onConfirmPressed: function (event) {
            const button = event.getSource();
            this.onEventButtonPressed("02", false, null, null, button);
        },
        onCancelPressed: function (event) {
            const button = event.getSource();
            MessageBox.alert("Сигурни ли сте, че искате да продължите?", {
                title: "Внимание!",
                actions: ["Да", "Не"],
                onClose: (action) => {
                    if (action == "Да") {
                        this.onEventButtonPressed("09", false, null, null, button);
                    }
                }
            });
        },
        onPrintPressed: function () {
            // this.onEventButtonPressed("03", false);
            this.onOpenPrintDialog()
        },
        onFinishPressed: function () {
            //open print dialog
            //attach the following fun when OK button is pressed
            // this.onEventButtonPressed("04", true);
            //do this with static variable

            this.onOpenPrintDialog();
            this.isFinishButtonProcessActivated = true;


        },

        onEventButtonPressed: function (backEndEvent, sendEverything, printSetValues, callbackFn, currentButton) {
            currentButton && currentButton.setEnabled(false);
            const event = backEndEvent;
            const table = this.getView().byId("uiTableScale");
            const dataArr = table.getModel("table").getData().results;
            const headerDataObj = this.getView().getModel("header").getData();
            const selectedIndices = table.getSelectedIndices();
            const oModel = new oDataModel("/sap/opu/odata/sap/ZYL_YARD_UI_SRV", true);
            const blockStock = this.getDataFromLocalModel("blockStock");
            const destBin = this.getDataFromLocalModel("destinationBin");

            let requestBody = {};
            let arr = [];
            selectedIndices.forEach(index => {
                let itemToAdd = dataArr[index];
                itemToAdd.Task = event;
                if (!itemToAdd.QuanBag.Quan) {
                    itemToAdd.QuanBag.Quan = "0.000";
                }

                itemToAdd.BlockStock = blockStock;
                itemToAdd.DestBin = destBin;

                arr.push(itemToAdd);
            });

            requestBody.ShipDeliveryHeadNav = headerDataObj;
            requestBody.LabelPrintNav = printSetValues ? printSetValues : []
            //Check if everything needs to be saved!
            if (sendEverything) {
                let arrToSend = dataArr;
                arrToSend.forEach(item => {
                    item.Task = event;
                });
                requestBody.ShipDeliveryItemNav = arrToSend;
                arr.length = 1;
            } else {
                requestBody.ShipDeliveryItemNav = arr;
            }
            if (arr.length != 0) {
                sap.ui.core.BusyIndicator.show();
                oModel.create("/ShipDeliveryGenSet", requestBody, {
                    success: (s, res) => {
                        sap.ui.core.BusyIndicator.hide();
                        currentButton && currentButton.setEnabled(true);
                        callbackFn && callbackFn()
                        const results = s.ShipDeliveryItemNav.results;
                        if (results.length !== 0 && (backEndEvent === "01" || backEndEvent === "02")) {
                            let counter = 0;
                            selectedIndices.forEach(index => {
                                table.getModel("table").getData().results[index] = results[counter++];
                            });
                            table.getModel("table").refresh();

                        }
                        if (s.ShipDeliveryHeadNav) {
                            this.getView().setModel(new JSONModel(s.ShipDeliveryHeadNav), "header");
                            this.getView().getModel("header").refresh(true);
                        }
                        if (backEndEvent === "04") {
                            this.checkStatus("09");
                        }
                        this.handleMessageResponse(res);
                    },
                    error: err => {
                        sap.ui.core.BusyIndicator.hide();
                        currentButton && currentButton.setEnabled(true);
                        callbackFn && callbackFn()
                        this.handleMessageResponse(err);
                    }
                });
            } else {
                currentButton && currentButton.setEnabled(true);
                MessageToast.show("Не са избрани редове.")
            }
        },

        getInputAutoCompleteData: function () {
            const oModel = new oDataModel("/sap/opu/odata/sap/ZYL_YARD_UI_SRV", true);
            const lgNum = "DEV1";
            let lgNumFilter = new Filter("Lgnum", "EQ", lgNum);
            oModel.read("/SearchHelp_BinSet", {
                filters: [lgNumFilter],
                success: (data) => {
                    const jsonModel = new JSONModel({ inputHelpModel: data.results });
                    this.getView().setModel(jsonModel, "inputHelpModel");
                },
                error: (error) => {
                    this.handleMessageResponse(error);
                }
            })
        },

        checkStatus: function (status) {
            const finishedStatus = "09";
            const items = {
                "weightButton": this.getView().byId("weightButton"),
                "taskButton": this.getView().byId("taskButton"),
                "confirmButton": this.getView().byId("confirmButton"),
                "topTareButton": this.getView().byId("topTareButton"),
                "topMeasureButton": this.getView().byId("topMeasureButton"),
                "finishButton": this.getView().byId("finishButton"),
                "cancelButton": this.getView().byId("cancelButton"),
            }

            if (status === finishedStatus) {
                for (let key in items) {
                    items[key].setEnabled(false);
                }
            } else {
                for (let key in items) {
                    items[key].setEnabled(true);

                }
            }
        },

        onOpenPrintDialog: function () {
            const view = this.getView();
            const popupDialogPrint = this.byId("popupDialogPrint")
            if (!popupDialogPrint) {
                Fragment.load({
                    id: view.getId(),
                    name: "zfioriscaletwo.view.dialogs.popupDialogPrint",
                    controller: this
                }).then((popupDialogPrint) => {
                    view.addDependent(popupDialogPrint);
                    popupDialogPrint.open();
                });
            } else {
                this.byId("popupDialogPrint").open();
            }
        },

        afterOpenPrintDialog: function () {

            let applId = new sap.ui.model.Filter("ApplId", "EQ", "SHIP_DELIVERY");

            const oModel = new oDataModel("/sap/opu/odata/sap/ZYL_YARD_UI_SRV", true);
            oModel.read("/LabelPrintCollection", {
                filters: [applId],
                success: (s, res) => {
                    sap.ui.core.BusyIndicator.hide();
                    this.getView().byId("uiTablePrint").setModel(new JSONModel(s), "tablePrint")

                },
                error: err => {
                    sap.ui.core.BusyIndicator.hide(0);
                    this.handleMessageResponse(err);
                }
            });
        },

        onClosePrint: function () {
            //create request
            //send checked only from uiTablePrint table with tablePrint model
            const table = this.getView().byId("uiTablePrint")
            const dataArr = table.getModel("tablePrint").getData().results;
            const selectedIndices = table.getSelectedIndices();
            let arr = [];
            selectedIndices.forEach(index => {
                let itemToAdd = dataArr[index];
                arr.push(itemToAdd);
            });
            const selectedIndScaleTable = this.getView().byId("uiTableScale").getSelectedIndices()
            const startFinishAndPrintProcess = (sendEverything) => {
                if (this.isFinishButtonProcessActivated) {
                    this.isFinishButtonProcessActivated = false;
                    //This is finish cb
                    const callbackFn = this.onEventButtonPressed.bind(this, "04", true)
                    const finishButton = this.byId("finishButton")
                    this.onEventButtonPressed("03", sendEverything, arr, callbackFn, finishButton);
                    this.byId("popupDialogPrint").close()
                } else {
                    //This is print
                    const printButton = this.byId("printButton")
                    this.onEventButtonPressed("03", sendEverything, arr, null, printButton);
                    this.byId("popupDialogPrint").close()
                }
            }
            if (selectedIndScaleTable.length >= 1) {
                startFinishAndPrintProcess(false)
            } else {
                startFinishAndPrintProcess(true)
            }
        },

        onCloseNoActionPrint: function () {
            this.byId("popupDialogPrint").close()
            if (this.isFinishButtonProcessActivated) {
                this.isFinishButtonProcessActivated = false;
                MessageToast.show("Задачата не е приключена!")
            }
        },

        fetchTareDataJSON: async function () {
            const tareDNS = this.getDataFromLocalModel("selectedScaleKeyDNS");
            const tareUrl = `http://${tareDNS}.int.agropolychim.bg:8891/currentWeight`
            try {
                const response = await fetch(tareUrl);
                if (!response.ok) {
                    const message = `An error has occured: ${response.status}`;
                    throw new Error(message);
                }
                let data = await response.json();
                if (data) {
                    let dataArr = data.currentWeight.split('');
                    let reversedArr = [...dataArr].reverse()
                    let lastDigit = dataArr[dataArr.length - 1]

                    function isCharNumberExceptZero(c) {
                        return c >= '0' && c <= '9';
                    }
                    //probably risky check
                    if (isCharNumberExceptZero(lastDigit)) {
                        for (const ele of reversedArr) {
                            if (isCharNumberExceptZero(ele)) {
                                dataArr.unshift(ele)
                                dataArr.pop()
                            } else {
                                break;
                            }
                        }
                        let dataJoined = dataArr.join('')
                        const weight = dataJoined.match(/(\d+)\n?/)[0]
                        data = { currentWeight: weight }
                    }
                }
                return data;
            } catch (error) {
                throw new Error(error);
            }
        },

        onTareSwitchChange: function (event, currentSetState) {
            const swtich = this.getView().byId("tareSwitch")

            if (typeof currentSetState === "boolean") {
                swtich.setState(currentSetState)
            }
            if (swtich.getState(true)) {
                //switch is true, testing if there is connecting to scale
                sap.ui.core.BusyIndicator.show(0);
                this.fetchTareDataJSON().then(() => {
                    //connection is positive, disabling inputs
                    sap.ui.core.BusyIndicator.hide();
                    swtich.setState(true)
                    if (this.isManualInputEnabled) {
                        switchInputs.call(this, true)
                    } else {
                        switchInputs.call(this, false)
                    }

                }).catch((error) => {
                    //error while connecting to scale, disabaling switch and enable inputs
                    sap.ui.core.BusyIndicator.hide();
                    sap.m.MessageToast.show("Има проблем с данните от кантара!");
                    swtich.setState(false)
                    switchInputs.call(this, true)
                });
            } else {
                //switch is false inputs are enabled
                switchInputs.call(this, true)
            }
            function switchInputs(value) {
                this.getView().byId("tareInput").setEditable(value)
                this.getView().byId("grossInput").setEditable(value)
                this.getView().byId("netInput").setEditable(value)
                const table = this.getView().byId("uiTableScale");
                table.getModel("table") && table.getModel("table").setProperty("/tableInputState", value)
            }
        },


    });
});
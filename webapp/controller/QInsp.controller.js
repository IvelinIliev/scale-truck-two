sap.ui.define([
    "./Base.controller",
    "../model/formatter",
    "sap/m/MessageToast",
    "sap/m/MessageBox",
    "sap/ui/model/odata/ODataModel",
    "sap/ui/model/json/JSONModel",
    "sap/ui/model/Filter",
    "sap/ui/core/Fragment"
], function (Base, formatter, MessageToast, MessageBox, oDataModel, JSONModel, Filter, Fragment) {

    "use strict";

    return Base.extend("zfioriscalewo.controller.QInsp", {

        formatter: formatter,
        freightOrderCreated: false,

        onInit: function () {
            let currentRouter = sap.ui.core.UIComponent.getRouterFor(this);
            let tableInputs = {
                "quantityBagInputTable": this.getView().byId("quantityBagInputTable"),
                "quantityDocInputTable": this.getView().byId("quantityDocInputTable")
            }
            Object.values(tableInputs).forEach(input => {
                input.addEventDelegate({
                    onfocusout: (evt) => {
                        const elementId = evt.target.id;
                        this.recalculateInputData(elementId);
                    }
                })
            })
            currentRouter.getRoute("qinsp").attachPatternMatched(this.onRouteMatched, this);
        },

        recalculateInputData: function (elementId) {
            sap.ui.core.BusyIndicator.show(0);
            const oModel = new oDataModel("/sap/opu/odata/sap/ZYL_YARD_UI_SRV", true);
            const tableModel = this.getView().getModel("table");
            const table = this.getView().byId("uiTableScale");
            let requestBody = {};
            let itemData = tableModel.getData().results[0];
            if (elementId.includes("quantityBag")) {
                itemData.QuanDoc.Quan = "0";
            } else {
                itemData.QuanBag.Quan = "0";
            }
            requestBody = itemData

            oModel.create("/ShipDeliveryItemSet", requestBody, {
                success: (s, res) => {
                    sap.ui.core.BusyIndicator.hide();
                    table.getModel("table").getData().results[0] = s;
                    table.getModel("table").refresh();
                    this.handleMessageResponse(res);
                },
                error: err => {
                    sap.ui.core.BusyIndicator.hide();
                    this.handleMessageResponse(err);
                }
            });

        },

        onRouteMatched: function (oEvent) {
            sap.ui.core.BusyIndicator.show(0);
            const rowData = JSON.parse(oEvent.getParameter("arguments").rowData);
            const torKey = this.getDataFromLocalModel("torKey");

            this.checkStatus(rowData[1]);
            this.removeCssFromTable();
            this.freightOrderCreated = false;

            if (torKey) {
                this.checkStatus(rowData[1]);
                this.getInputAutoCompleteData();

                this.getDataFromServer(rowData[0], torKey).then(serverData => {
                    // read data for current FO
                    const jsonModelHeader = new JSONModel(serverData.ShipDeliveryHeadNav);
                    const jsonModelTable = new JSONModel(serverData.ShipDeliveryItemNav);
                    this.getView().setModel(jsonModelHeader, "header");
                    this.getView().setModel(jsonModelTable, "table");
                    this.initSelectedWhNumber()

                    // create new internal movement FO
                    this.sendCreateFreightOrder();
                    sap.ui.core.BusyIndicator.hide();

                    if (window.location.host.includes("app")) {
                        this.onTareSwitchChange(null, true)
                    }
                }).catch(err => {
                    this.onNavBack();
                    sap.ui.core.BusyIndicator.hide();
                });

                this.checkTableForConfirmedTask();

            } else {
                this.onNavBack();
                sap.ui.core.BusyIndicator.hide();
            }
        },

        checkTableForConfirmedTask: function () {
            const uiTable = this.getView().byId("uiTableScale");
            if (!uiTable.mEventRegistry.rowSelectionChange) {
                uiTable.attachRowSelectionChange((event) => {
                    let table = event.getSource();
                    table.getSelectedIndices().forEach(index => {
                        let isItemBlocked = table.getRows()[index].getBindingContext("table").getObject("BlockItem");
                        if (isItemBlocked) {
                            table.setSelectedIndex(-1)
                            table.getRows()[index]._setSelected(false);
                            MessageToast.show("За продукта има подтвърдена задача!");
                        }
                    });
                });
            }
        },

        onNavBack: function () {
            const headerModel = this.getView().getModel("header");

            if (this.freightOrderCreated && headerModel.oData.GrossWei.Quan == "0.000") {
                // display pop-up for FO cancellation
                MessageBox.confirm("Сигурни ли сте, че искате да канселирате документа?", (oAction) => {
                    if (oAction === sap.m.MessageBox.Action.OK) {
                        this.freightOrderCreated = false;
                        // send request to delete FO
                        this.onEventButtonPressed("11", true);
                        // go back to the home page
                        this.onNavBack();
                    }
                }, "Confirmation");
            } else {
                const oRouter = sap.ui.core.UIComponent.getRouterFor(this);
                oRouter.navTo("home");
            }
        },

        getDataFromLocalModel: function (key) {
            const localModel = this.getView().getModel("localModel");
            let currentObject = localModel.getData();
            return currentObject[key];
        },

        getDataFromServer: function (lgNum, torKey) {
            return new Promise((resolve, reject) => {
                //---------------------
                const oModel = new sap.ui.model.odata.ODataModel("/sap/opu/odata/sap/ZYL_YARD_UI_SRV", true);
                let setInfo = "(Lgnum='" + lgNum + "',TorKey=guid'" + torKey + "')"
                oModel.read("/ShipDeliveryGenSet" + setInfo, {
                    urlParameters: {
                        "$expand": "ShipDeliveryHeadNav,ShipDeliveryItemNav"
                    },
                    success: (data, res) => {
                        this.handleMessageResponse(res);
                        sap.ui.core.BusyIndicator.hide();
                        resolve(data);
                    },
                    error: err => {
                        sap.ui.core.BusyIndicator.hide();
                        this.handleMessageResponse(err);
                        reject(err);

                    }
                });
                //---------------------
            });
        },

        onWriteButtonPress: function () {
            this.sendHeaderDataToServerWithPostRequest();
        },

        sendHeaderDataToServerWithPostRequest: function () {
            const oModel = new oDataModel("/sap/opu/odata/sap/ZYL_YARD_UI_SRV", true);
            const headerModel = this.getView().getModel("header");
            let setInfo = "";
            let requestBody = headerModel.getData();

            oModel.create("/ShipDeliveryHeadSet" + setInfo, requestBody, {
                success: (s, res) => {
                    this.getView().setModel(new JSONModel(s), "header");
                    this.handleMessageResponse(res);
                },
                error: err => {
                    this.handleMessageResponse(err);
                }
            });
        },

        onTarePress: function () {
            sap.ui.core.BusyIndicator.show(0);
            const switchState = this.getView().byId("tareSwitch").getState()
            if (switchState) {
                this.fetchTareDataJSON().then(data => {
                    // When fetch succeeds
                    //send data to backend
                    const weight = data && data.currentWeight.match(/(\d+)\n?(\+?\-?)/)[0]
                    if (weight == "999") {
                        sap.ui.core.BusyIndicator.hide(0);
                        sap.m.MessageToast.show("Неконсистентни данни от кантара! Опитай отново!");
                    } else {
                        this.sendHeaderSetRequest(weight);
                    }

                }).catch(error => {
                    sap.ui.core.BusyIndicator.hide();
                    sap.m.MessageToast.show("Има проблем с данните от кантара!");
                });
            } else {
                const tareInput = this.getView().byId("tareInput");
                this.sendHeaderSetRequest(tareInput.getValue());

            }
        },
        sendHeaderSetRequest: function (scaleWeight) {
            const headerModel = this.getView().getModel("header");
            let torKey = headerModel.getData().TorKey;
            let lgNum = this.currentChosenWarehouseNumber;
            const weight = scaleWeight ? scaleWeight : "0"
            // new req ScaleQty
            const oModel = new oDataModel("/sap/opu/odata/sap/ZYL_YARD_UI_SRV", true);
            let setInfo = "(Lgnum='" + lgNum + "',TorKey=guid'" + torKey + "',ScaleQty='" + weight + "')";

            const tareButton = this.byId("topTareButton");
            tareButton.setEnabled(false);
            oModel.read("/ShipDeliveryHeadSet" + setInfo, {
                success: (s, res) => {
                    tareButton.setEnabled(true);
                    sap.ui.core.BusyIndicator.hide();
                    this.handleMessageResponse(res);
                    this.setTareValues(s);

                },
                error: err => {
                    tareButton.setEnabled(true);
                    sap.ui.core.BusyIndicator.hide(0);
                    this.handleMessageResponse(err);
                }
            });
        },


        setTareValues: function (values) {

            sap.ui.core.BusyIndicator.hide();
            const tareInput = this.getView().byId("tareInput");
            const grossInput = this.getView().byId("grossInput");
            const netInput = this.getView().byId("netInput");

            tareInput.setValue(values.TareWei.Quan);
            //tareInput.setDescription(data.TareWei.Unit);
            grossInput.setValue(values.GrossWei.Quan);
            //grossInput.setDescription(data.GrossWei.Unit);
            netInput.setValue(values.NetWei.Quan);
            //netInput.setDescription(data.NetWei.Unit);
        },


        onMeasuredPress: function () {
            this.sendHeaderDataToServerWithPostRequest();
        },

        onWeightPressed: function () {
            const cssClasses = {
                "03": "red-row",
                "02": "yellow-row"
            }
            const backEndEvent = "06";

            const table = this.getView().byId("uiTableScale");
            const selectedIndices = table.getSelectedIndices();

            const headerDataObj = this.getView().getModel("header").getData();
            if (selectedIndices.length !== 1) {
                MessageToast.show("Избери 1 ред!")
            } else {
                const oModel = new oDataModel("/sap/opu/odata/sap/ZYL_YARD_UI_SRV", true);
                const switchState = this.getView().byId("tareSwitch").getState()
                const dataArr = table.getModel("table").getData().results;
                if (switchState) {
                    this.fetchTareDataJSON().then(data => {
                        // When fetch succeeds
                        let weight = data && data.currentWeight.match(/(\d+)\n?(\+?\-?)/)[0]
                        if (weight == "999") {
                            sap.ui.core.BusyIndicator.hide();
                            sap.m.MessageToast.show("Неконсистентни данни от кантара! Опитай отново!");
                            return;
                        }
                        dataArr[selectedIndices].QuanWei.Quan = weight
                        table.getModel("table").refresh();
                        let requestBody = {}

                        requestBody.ShipDeliveryHeadNav = headerDataObj;
                        let selectedItem = dataArr[selectedIndices];
                        selectedItem.Task = backEndEvent;
                        requestBody.ShipDeliveryItemNav = [selectedItem];
                        this.createTaskGenRequest(oModel, requestBody, table, selectedIndices, cssClasses);
                    }).catch(error => {
                        // When fetch ends with a bad http status
                        console.log("problem with fetching data " + error)
                        sap.ui.core.BusyIndicator.hide();
                        sap.m.MessageToast.show("Има проблем с данните от кантара!");
                    });
                } else {
                    let requestBody = {}
                    requestBody.ShipDeliveryHeadNav = headerDataObj;
                    let selectedItem = dataArr[selectedIndices];
                    selectedItem.Task = backEndEvent;
                    requestBody.ShipDeliveryItemNav = [selectedItem];
                    this.createTaskGenRequest(oModel, requestBody, table, selectedIndices, cssClasses);
                }
            }
        },

        createTaskGenRequest: function (oModel, requestBody, table, selectedIndices, cssClasses) {
            const weightButton = this.byId("weightButton");
            weightButton.setEnabled(false);
            oModel.create("/ShipDeliveryGenSet", requestBody, {
                success: (successData, response) => {
                    weightButton.setEnabled(true);
                    const itemData = successData.ShipDeliveryItemNav.results[0];
                    const css = itemData.TolerStat;
                    this.removeCssFromTable();
                    table.getRows()[selectedIndices].addStyleClass(cssClasses[css]);

                    this.getView().setModel(new JSONModel(successData.ShipDeliveryHeadNav), "header");
                    this.getView().getModel("header").refresh(true);
                    table.getModel("table").getData().results[selectedIndices] = itemData;
                    table.getModel("table").refresh();
                    this.handleMessageResponse(response)

                },
                error: err => {
                    weightButton.setEnabled(true)
                    this.handleMessageResponse(err)
                }
            });
        },

        removeCssFromTable: function () {
            const cssClasses = { "03": "red-row", "02": "yellow-row" }
            const table = this.getView().byId("uiTableScale");
            table.getRows().forEach(row => {
                row.removeStyleClass(cssClasses["02"]);
                row.removeStyleClass(cssClasses["03"]);
            });
        },

        onConfirmPressed: function () {
            this.onEventButtonPressed("02", false, true);
        },
        onPrintPressed: function () {
            //            this.onEventButtonPressed("03", false);
            this.onOpenPrintDialog();
        },
        onFinishPressed: function () {
            this.onEventButtonPressed("04", true, true);
        },

        onEventButtonPressed: function (backEndEvent, sendEverything, deleteFOCreatedFlag, printSetValues, callbackFn) {
            const event = backEndEvent;
            const table = this.getView().byId("uiTableScale");
            const dataArr = table.getModel("table").getData().results;
            const headerDataObj = this.getView().getModel("header").getData();
            const selectedIndices = table.getSelectedIndices();
            const oModel = new oDataModel("/sap/opu/odata/sap/ZYL_YARD_UI_SRV", true);
            let requestBody = {};
            let arr = [];
            selectedIndices.forEach(index => {
                let itemToAdd = dataArr[index];
                itemToAdd.Task = event;
                if (!itemToAdd.QuanBag.Quan) {
                    itemToAdd.QuanBag.Quan = "0.000";
                }
                arr.push(itemToAdd);
            });
            requestBody.ShipDeliveryHeadNav = headerDataObj;
            requestBody.LabelPrintNav = printSetValues ? printSetValues : []
            //Check if everything needs to be saved!
            if (sendEverything) {
                let arrToSend = dataArr;
                arrToSend.forEach(item => {
                    item.Task = event;
                });
                requestBody.ShipDeliveryItemNav = arrToSend;
                arr.length = 1;
            } else {
                requestBody.ShipDeliveryItemNav = arr;
            }
            if (arr.length != 0) {
                oModel.create("/ShipDeliveryGenSet", requestBody, {
                    success: (s, res) => {
                        const results = s.ShipDeliveryItemNav.results;
                        if (results.length !== 0 && (backEndEvent === "01" || backEndEvent === "02")) {
                            let counter = 0;
                            selectedIndices.forEach(index => {
                                table.getModel("table").getData().results[index] = results[counter++];
                            });
                            table.getModel("table").refresh();

                        }
                        if (s.ShipDeliveryHeadNav) {
                            this.getView().setModel(new JSONModel(s.ShipDeliveryHeadNav), "header");
                            this.getView().getModel("header").refresh(true);
                        }
                        if (backEndEvent === "04") {
                            this.checkStatus("09");
                        }
                        if (deleteFOCreatedFlag) {
                            // FO WTs was confirmed successfully => delete flag to allow backward navigation
                            this.freightOrderCreated = false;
                        }
                        this.handleMessageResponse(res);
                    },
                    error: err => {
                        this.handleMessageResponse(err);
                    }
                });
            } else {
                MessageToast.show("Не са избрани редове.")
            }
        },

        onOpenPrintDialog: function () {
            const view = this.getView();
            const popupDialogPrint = this.byId("popupDialogPrint")
            if (!popupDialogPrint) {
                Fragment.load({
                    id: view.getId(),
                    name: "zfioriscaletwo.view.dialogs.popupDialogPrint",
                    controller: this
                }).then((popupDialogPrint) => {
                    view.addDependent(popupDialogPrint);
                    popupDialogPrint.open();
                });
            } else {
                this.byId("popupDialogPrint").open();
            }
        },

        afterOpenPrintDialog: function () {

            let applId = new sap.ui.model.Filter("ApplId", "EQ", "SHIP_DELIVERY");

            const oModel = new oDataModel("/sap/opu/odata/sap/ZYL_YARD_UI_SRV", true);
            oModel.read("/LabelPrintCollection", {
                filters: [applId],
                success: (s, res) => {
                    sap.ui.core.BusyIndicator.hide();
                    this.getView().byId("uiTablePrint").setModel(new JSONModel(s), "tablePrint")

                },
                error: err => {
                    sap.ui.core.BusyIndicator.hide(0);
                    this.handleMessageResponse(err);
                }
            });
        },

        onClosePrint: function () {
            //create request
            //send checked only from uiTablePrint table with tablePrint model
            const table = this.getView().byId("uiTablePrint")
            const dataArr = table.getModel("tablePrint").getData().results;
            const selectedIndices = table.getSelectedIndices();
            let arr = [];
            selectedIndices.forEach(index => {
                let itemToAdd = dataArr[index];
                arr.push(itemToAdd);
            });
            const selectedIndScaleTable = this.getView().byId("uiTableScale").getSelectedIndices()
            const startFinishAndPrintProcess = (sendEverything) => {
                if (this.isFinishButtonProcessActivated) {
                    this.isFinishButtonProcessActivated = false;
                    //This is finish cb
                    const callbackFn = this.onEventButtonPressed.bind(this, "04", true)
                    this.onEventButtonPressed("03", sendEverything, false, arr, callbackFn);
                    this.byId("popupDialogPrint").close()
                } else {
                    //This is print
                    this.onEventButtonPressed("03", sendEverything, false, arr);
                    this.byId("popupDialogPrint").close()
                }
            }
            if (selectedIndScaleTable.length >= 1) {
                startFinishAndPrintProcess(false)
            } else {
                startFinishAndPrintProcess(true)
            }
        },

        onCloseNoActionPrint: function () {
            this.byId("popupDialogPrint").close()
            if (this.isFinishButtonProcessActivated) {
                this.isFinishButtonProcessActivated = false;
                MessageToast.show("Задачата не е приключена!")
            }
        },

        getInputAutoCompleteData: function () {
            const oModel = new oDataModel("/sap/opu/odata/sap/ZYL_YARD_UI_SRV", true);
            const lgNum = "DEV1";
            let lgNumFilter = new Filter("Lgnum", "EQ", lgNum);
            oModel.read("/SearchHelp_BinSet", {
                filters: [lgNumFilter],
                success: (data) => {
                    const jsonModel = new JSONModel({ inputHelpModel: data.results });
                    this.getView().setModel(jsonModel, "inputHelpModel");
                },
                error: (error) => {
                    this.handleMessageResponse(error);
                }
            })
        },

        checkStatus: function (status) {
            const finishedStatus = "09";
            const items = {
                "weightButton": this.getView().byId("weightButton"),
                "confirmButton": this.getView().byId("confirmButton"),
                "topTareButton": this.getView().byId("topTareButton"),
                "topMeasureButton": this.getView().byId("topMeasureButton"),
                "finishButton": this.getView().byId("finishButton")
            }

            if (status === finishedStatus) {
                for (let key in items) {
                    items[key].setEnabled(false);
                }
            } else {
                for (let key in items) {
                    items[key].setEnabled(true);

                }
            }
        },

        sendCreateFreightOrder: function () {
            const oModel = new oDataModel("/sap/opu/odata/sap/ZYL_YARD_UI_SRV", true);
            const table = this.getView().byId("uiTableScale");
            const dataArr = table.getModel("table").getData().results;
            const headerDataObj = this.getView().getModel("header").getData();
            const createFOStatus = "10";
            const destBin = this.getDataFromLocalModel("destinationBin");
            const blockStock = this.getDataFromLocalModel("blockStock");

            dataArr.forEach(item => {
                item.Task = createFOStatus;
                item.DestBin = destBin;
                item.BlockStock = blockStock;
            });

            let requestBody = {};
            requestBody.ShipDeliveryHeadNav = headerDataObj;
            requestBody.ShipDeliveryItemNav = dataArr;

            oModel.create("/ShipDeliveryGenSet", requestBody, {
                success: (s, res) => {
                    const itemData = s.ShipDeliveryItemNav.results[0];
                    this.getView().setModel(new JSONModel(s.ShipDeliveryHeadNav), "header");
                    this.getView().getModel("header").refresh(true);
                    table.getModel("table").getData().results[0] = itemData;
                    table.getModel("table").refresh();
                    this.freightOrderCreated = true;
                    this.handleMessageResponse(res);
                },
                error: err => {
                    const oRouter = sap.ui.core.UIComponent.getRouterFor(this);
                    this.handleMessageResponse(err, oRouter, "home");
                }
            });
        },

        onTareSwitchChange: function (event, currentSetState) {
            const swtich = this.getView().byId("tareSwitch")

            if (typeof currentSetState === "boolean") {
                swtich.setState(currentSetState)
            }

            if (swtich.getState(currentState)) {
                //switch is true, testing if there is connecting to scale
                sap.ui.core.BusyIndicator.show(0);
                this.fetchTareDataJSON().then(() => {
                    //connection is positive, disabling inputs
                    sap.ui.core.BusyIndicator.hide();
                    swtich.setState(true)
                    if (this.isManualInputEnabled) {
                        switchInputs.call(this, true)
                    } else {
                        switchInputs.call(this, false)
                    }

                }).catch((error) => {
                    //error while connecting to scale, disabaling switch and enable inputs
                    sap.ui.core.BusyIndicator.hide();
                    sap.m.MessageToast.show("Има проблем с данните от кантара!");
                    swtich.setState(false)
                    switchInputs.call(this, true)
                });
            } else {
                //switch is false inputs are enabled
                switchInputs.call(this, true)
            }
            function switchInputs(value) {
                this.getView().byId("tareInput").setEditable(value)
                this.getView().byId("grossInput").setEditable(value)
                this.getView().byId("netInput").setEditable(value)
                const table = this.getView().byId("uiTableScale");
                table.getModel("table") && table.getModel("table").setProperty("/tableInputState", value)
            }
        },

        fetchTareDataJSON: async function () {
            const tareDNS = this.getDataFromLocalModel("selectedScaleKeyDNS");
            const tareUrl = `http://${tareDNS}.int.agropolychim.bg:8891/currentWeight`
            try {
                const response = await fetch(tareUrl);
                if (!response.ok) {
                    const message = `An error has occured: ${response.status}`;
                    throw new Error(message);
                }
                let data = await response.json();
                if (data) {
                    let dataArr = data.currentWeight.split('');
                    let reversedArr = [...dataArr].reverse()
                    let lastDigit = dataArr[dataArr.length - 1]

                    function isCharNumberExceptZero(c) {
                        return c >= '0' && c <= '9';
                    }
                    //probably risky check
                    if (isCharNumberExceptZero(lastDigit)) {
                        for (const ele of reversedArr) {
                            if (isCharNumberExceptZero(ele)) {
                                dataArr.unshift(ele)
                                dataArr.pop()
                            } else {
                                break;
                            }
                        }
                        let dataJoined = dataArr.join('')
                        const weight = dataJoined.match(/(\d+)\n?/)[0]
                        data = { currentWeight: weight }
                    }
                }
                return data;
            } catch (error) {
                throw new Error(error);
            }
        },

    });
});
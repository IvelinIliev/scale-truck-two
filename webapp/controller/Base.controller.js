sap.ui.define([
    "sap/ui/core/mvc/Controller",
    "sap/ui/core/UIComponent",
    "sap/m/MessageBox",
], function (Controller, UIComponent, MessageBox) {
    "use strict";

    return Controller.extend("zfioriscale.controller.Base", {

        currentChosenWarehouseNumber: undefined,

        onInit: function () {
            setTimeout(() => { this.initSelectedWhNumber(); }, 0)

        },

        initSelectedWhNumber: function () {
            const localStorageNumber = localStorage.getItem("selectedWhNumber")
            const comboBox = this.getView().byId("warehouseComboBox");
            const defaultValue = "DEV1"
            if (localStorageNumber) {
                this.currentChosenWarehouseNumber = localStorageNumber
            } else {
                this.currentChosenWarehouseNumber = defaultValue
            }
            comboBox && comboBox.setSelectedKey(this.currentChosenWarehouseNumber)
        },

        onComboBoxSelectionChangedWarehouseNumber: function () {
            const comboBox = this.getView().byId("warehouseComboBox");
            let newNumber = comboBox.getSelectedItem().getText();
            window.localStorage.setItem("selectedWhNumber", newNumber);
            this.currentChosenWarehouseNumber = newNumber;
        },


        handleMessageResponse: function (response, oRouter, navToPage) {

            const errorCodeInvalidRange = "9FB7E23B75B7157FE10000000A11447B";

            if (!response.headers) {
                response = response.response;
            }
            let isThereSuccessMessage = !!response.headers["sap-message"];
            if (isThereSuccessMessage) {
                let sapMessages = JSON.parse(response.headers["sap-message"]);
                let severity = sapMessages.severity;
                let newIcon = MessageBox.Icon.SUCCESS;
                if (severity === "error") {
                    newIcon = MessageBox.Icon.ERROR
                } else if (severity === "warning") {
                    newIcon = MessageBox.Icon.WARNING
                } else if (severity === "info") {
                    newIcon = MessageBox.Icon.INFORMATION
                }
                MessageBox.show(
                    sapMessages.message, {
                    icon: newIcon,
                    title: severity[0].toUpperCase() + severity.slice(1),
                    styleClass: "sapUiSizeCompact"
                });
            } else {
                response.body ? response.body : response.body = response.responseText
                if (response.body.includes("error")) {
                    try {
                        let error = JSON.parse(response.body);
                        let message = error.error.message.value;
                        if (error.error.code.includes(errorCodeInvalidRange)) {
                            message = "Въведена е невалидна стойност. Изпозлвайте формат : 00.000";
                        }
                        MessageBox.error(message);
                    } catch (err) {
                        if (err.name.includes("Syntax")) {
                            const parser = new DOMParser();
                            const xmlDoc = parser.parseFromString(response.body, "text/xml");
                            const newErrorXml = xmlDoc.getElementsByTagName("message")[0].innerHTML;
                            sap.m.MessageToast.show(newErrorXml);
                        } else {
                            sap.m.MessageToast.show("Неочакван отговор от системата!")
                        }

                    }
                }
            }
        },

        saveDataToLocalModel: function (key, value) {
            const localModel = this.getView().getModel("localModel");
            let currentObject = localModel.getData();
            currentObject[key] = value;
            localModel.setData(currentObject);
        },

        getDataFromLocalModel: function (key) {
            const localModel = this.getView().getModel("localModel");
            let currentObject = localModel.getData();
            return currentObject[key];
        },

        onColumnMenuOpen: function (evt) {
            evt.getSource().setFilterValue("");
        }

    });

});